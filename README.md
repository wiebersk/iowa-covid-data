## Iowa Covid Extractor

This project extracts data using the Domo integration that is used by the [Covid Iowa](https://coronavirus.iowa.gov/pages/case-counts) charts to extract:

- Daily tests
- Daily individuals tested
- Daily deaths

This data is exported from the daily bar charts for individuals, tests and deaths. 

### Files 

- `county-totals-yyyy-mm-dd.csv` - This file has total, positive and negative individual and test counts for every day since 2020-03-11 for each county. There is one column per county, metric and one row for each day.
- `state-totals-yyyy-mm-dd.csv` - This file has total, positive and negative individual and test counts for every day since 2020-03-11 for the state.

These files will be triggered everyday at 10:55 AM US Central time.