require 'date'

module CovidIowa
  module Constants
    TOKEN_URL = 'https://public.domo.com/embed/pages/OYPXR'
    DEATHS_TOKEN_URL = 'https://public.domo.com/embed/pages/VO7VW'
    
    TOTAL_TESTS_URL = 'https://public.domo.com/embed/pages/OYPXR/cards/1437646689/render?parts=dynamic,summary&locale=en-US'
    TOTAL_POSITIVE_TESTS_URL = 'https://public.domo.com/embed/pages/OYPXR/cards/642741910/render?parts=dynamic,summary&locale=en-US'
    TOTAL_NEGATIVE_TESTS_URL = 'https://public.domo.com/embed/pages/OYPXR/cards/103123728/render?parts=dynamic,summary&locale=en-US'

    TOTAL_INDIVIDUALS_URL = 'https://public.domo.com/embed/pages/OYPXR/cards/394856180/render?parts=dynamic,summary&locale=en-US'
    TOTAL_POSITIVE_INDIVIDUALS_URL = 'https://public.domo.com/embed/pages/OYPXR/cards/1481517716/render?parts=dynamic,summary&locale=en-US'
    TOTAL_NEGATIVE_INDIVIDUALS_URL = 'https://public.domo.com/embed/pages/OYPXR/cards/217503478/render?parts=dynamic,summary&locale=en-US'

    TOTAL_DEATHS_URL = 'https://public.domo.com/embed/pages/VO7VW/cards/918996746/render?parts=dynamic,summary,annotations&locale=en-US'
    
    START_DATE = '2020-03-11'
    DATE_RANGE = (Date.parse(START_DATE)..Date.today)

    FIPS_CODES = {"19001"=>{:name=>"Adair", :code=>"19001"}, "19003"=>{:name=>"Adams", :code=>"19003"}, "19005"=>{:name=>"Allamakee", :code=>"19005"}, "19007"=>{:name=>"Appanoose", :code=>"19007"}, "19009"=>{:name=>"Audubon", :code=>"19009"}, "19011"=>{:name=>"Benton", :code=>"19011"}, "19013"=>{:name=>"Black Hawk", :code=>"19013"}, "19015"=>{:name=>"Boone", :code=>"19015"}, "19017"=>{:name=>"Bremer", :code=>"19017"}, "19019"=>{:name=>"Buchanan", :code=>"19019"}, "19021"=>{:name=>"Buena Vista", :code=>"19021"}, "19023"=>{:name=>"Butler", :code=>"19023"}, "19025"=>{:name=>"Calhoun", :code=>"19025"}, "19027"=>{:name=>"Carroll", :code=>"19027"}, "19029"=>{:name=>"Cass", :code=>"19029"}, "19031"=>{:name=>"Cedar", :code=>"19031"}, "19033"=>{:name=>"Cerro Gordo", :code=>"19033"}, "19035"=>{:name=>"Cherokee", :code=>"19035"}, "19037"=>{:name=>"Chickasaw", :code=>"19037"}, "19039"=>{:name=>"Clarke", :code=>"19039"}, "19041"=>{:name=>"Clay", :code=>"19041"}, "19043"=>{:name=>"Clayton", :code=>"19043"}, "19045"=>{:name=>"Clinton", :code=>"19045"}, "19047"=>{:name=>"Crawford", :code=>"19047"}, "19049"=>{:name=>"Dallas", :code=>"19049"}, "19051"=>{:name=>"Davis", :code=>"19051"}, "19053"=>{:name=>"Decatur", :code=>"19053"}, "19055"=>{:name=>"Delaware", :code=>"19055"}, "19057"=>{:name=>"Des Moines", :code=>"19057"}, "19059"=>{:name=>"Dickinson", :code=>"19059"}, "19061"=>{:name=>"Dubuque", :code=>"19061"}, "19063"=>{:name=>"Emmet", :code=>"19063"}, "19065"=>{:name=>"Fayette", :code=>"19065"}, "19067"=>{:name=>"Floyd", :code=>"19067"}, "19069"=>{:name=>"Franklin", :code=>"19069"}, "19071"=>{:name=>"Fremont", :code=>"19071"}, "19073"=>{:name=>"Greene", :code=>"19073"}, "19075"=>{:name=>"Grundy", :code=>"19075"}, "19077"=>{:name=>"Guthrie", :code=>"19077"}, "19079"=>{:name=>"Hamilton", :code=>"19079"}, "19081"=>{:name=>"Hancock", :code=>"19081"}, "19083"=>{:name=>"Hardin", :code=>"19083"}, "19085"=>{:name=>"Harrison", :code=>"19085"}, "19087"=>{:name=>"Henry", :code=>"19087"}, "19089"=>{:name=>"Howard", :code=>"19089"}, "19091"=>{:name=>"Humboldt", :code=>"19091"}, "19093"=>{:name=>"Ida", :code=>"19093"}, "19095"=>{:name=>"Iowa", :code=>"19095"}, "19097"=>{:name=>"Jackson", :code=>"19097"}, "19099"=>{:name=>"Jasper", :code=>"19099"}, "19101"=>{:name=>"Jefferson", :code=>"19101"}, "19103"=>{:name=>"Johnson", :code=>"19103"}, "19105"=>{:name=>"Jones", :code=>"19105"}, "19107"=>{:name=>"Keokuk", :code=>"19107"}, "19109"=>{:name=>"Kossuth", :code=>"19109"}, "19111"=>{:name=>"Lee", :code=>"19111"}, "19113"=>{:name=>"Linn", :code=>"19113"}, "19115"=>{:name=>"Louisa", :code=>"19115"}, "19117"=>{:name=>"Lucas", :code=>"19117"}, "19119"=>{:name=>"Lyon", :code=>"19119"}, "19121"=>{:name=>"Madison", :code=>"19121"}, "19123"=>{:name=>"Mahaska", :code=>"19123"}, "19125"=>{:name=>"Marion", :code=>"19125"}, "19127"=>{:name=>"Marshall", :code=>"19127"}, "19129"=>{:name=>"Mills", :code=>"19129"}, "19131"=>{:name=>"Mitchell", :code=>"19131"}, "19133"=>{:name=>"Monona", :code=>"19133"}, "19135"=>{:name=>"Monroe", :code=>"19135"}, "19137"=>{:name=>"Montgomery", :code=>"19137"}, "19139"=>{:name=>"Muscatine", :code=>"19139"}, "19141"=>{:name=>"O'Brien", :code=>"19141"}, "19143"=>{:name=>"Osceola", :code=>"19143"}, "19145"=>{:name=>"Page", :code=>"19145"}, "19147"=>{:name=>"Palo Alto", :code=>"19147"}, "19149"=>{:name=>"Plymouth", :code=>"19149"}, "19151"=>{:name=>"Pocahontas", :code=>"19151"}, "19153"=>{:name=>"Polk", :code=>"19153"}, "19155"=>{:name=>"Pottawattamie", :code=>"19155"}, "19157"=>{:name=>"Poweshiek", :code=>"19157"}, "19159"=>{:name=>"Ringgold", :code=>"19159"}, "19161"=>{:name=>"Sac", :code=>"19161"}, "19163"=>{:name=>"Scott", :code=>"19163"}, "19165"=>{:name=>"Shelby", :code=>"19165"}, "19167"=>{:name=>"Sioux", :code=>"19167"}, "19169"=>{:name=>"Story", :code=>"19169"}, "19171"=>{:name=>"Tama", :code=>"19171"}, "19173"=>{:name=>"Taylor", :code=>"19173"}, "19175"=>{:name=>"Union", :code=>"19175"}, "19177"=>{:name=>"Van Buren", :code=>"19177"}, "19179"=>{:name=>"Wapello", :code=>"19179"}, "19181"=>{:name=>"Warren", :code=>"19181"}, "19183"=>{:name=>"Washington", :code=>"19183"}, "19185"=>{:name=>"Wayne", :code=>"19185"}, "19187"=>{:name=>"Webster", :code=>"19187"}, "19189"=>{:name=>"Winnebago", :code=>"19189"}, "19191"=>{:name=>"Winneshiek", :code=>"19191"}, "19193"=>{:name=>"Woodbury", :code=>"19193"}, "19195"=>{:name=>"Worth", :code=>"19195"}, "19197"=>{:name=>"Wright", :code=>"19197"}}

    DEFAULT_BODY = { "queryOverrides": { } }
    
    def fips_filter fips_code
      {
        "queryOverrides": {
          "filters": [
            {
              "column": "pop_FIPS",
              "operand": "IN",
              "values": [
                fips_code
              ],
              "label": "pop_FIPS"
            }
          ]
        }
      }
    end
  end
end