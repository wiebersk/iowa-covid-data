require_relative 'extractor.rb'

module CovidIowa
  class IndividualsExtractor < Extractor

    def extract county_fips = nil
      county_fips ? get_county_totals(county_fips) : get_state_totals
    end

    private

    def get_state_totals
      @data['iowa'] = {} unless @data.has_key?('iowa')
      @data['iowa']['individuals_tested'] = formatted_response(TOTAL_INDIVIDUALS_URL)

      @data['iowa']['individuals_positive'] = formatted_response(TOTAL_POSITIVE_INDIVIDUALS_URL)

      @data['iowa']['individuals_negative'] = formatted_response(TOTAL_NEGATIVE_INDIVIDUALS_URL)

      @data
    end

    def get_county_totals fips
      hsh = {}

      hsh['individuals_tested'] = formatted_response(TOTAL_INDIVIDUALS_URL, fips)

      hsh['individuals_positive'] = formatted_response(TOTAL_POSITIVE_INDIVIDUALS_URL, fips)

      hsh['individuals_negative'] = formatted_response(TOTAL_NEGATIVE_INDIVIDUALS_URL, fips)

      county_transpose_response(hsh, fips)
      hsh
    end
  end
end