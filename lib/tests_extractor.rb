require_relative 'extractor.rb'

module CovidIowa
  class TestsExtractor < Extractor

    def extract county_fips = nil
      county_fips ? get_county_totals(county_fips) : get_state_totals
    end

    private

    def get_state_totals
      @data['iowa'] = {} unless @data.has_key?('iowa')
      @data['iowa']['total_tests'] = formatted_response(TOTAL_TESTS_URL)

      @data['iowa']['positive_tests'] = formatted_response(TOTAL_POSITIVE_TESTS_URL)

      @data['iowa']['negative_tests'] = formatted_response(TOTAL_NEGATIVE_TESTS_URL)

      @data
    end

    def get_county_totals fips
      hsh = {}

      hsh['total_tests'] = formatted_response(TOTAL_TESTS_URL, fips)
      hsh['positive_tests'] = formatted_response(TOTAL_POSITIVE_TESTS_URL, fips)
      hsh['negative_tests'] = formatted_response(TOTAL_NEGATIVE_TESTS_URL, fips)

      county_transpose_response(hsh, fips)
      hsh
    end
  end
end