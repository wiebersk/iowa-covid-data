require_relative 'extractor.rb'

module CovidIowa
  class DeathsExtractor < Extractor

    def extract
      get_state_totals
    end

    private

    def get_state_totals
      @data['iowa'] = {} unless @data.has_key?('iowa')
      @data['iowa']['deaths'] = formatted_response(TOTAL_DEATHS_URL)

      @data
    end

    private
    def get_token
      token = /x-domo-embed-token': '(.*)'/.match(HTTParty.get(DEATHS_TOKEN_URL).body)[1]
    end
  end
end