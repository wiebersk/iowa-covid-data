require_relative 'constants.rb'
require 'httparty'

module CovidIowa
  class Extractor
    include CovidIowa::Constants

    attr_accessor :token, :data

    def initialize
      @data = {}
      @token = get_token
    end

    def formatted_response url, county_fips = nil
      case county_fips
      when nil
        get_data_from_response(HTTParty.put(url, body: DEFAULT_BODY.to_json, headers: { 'Content-Type' => 'application/json', 'x-domo-embed-token' => @token } ))
      else 
        get_data_from_response(HTTParty.put(url, body: fips_filter(county_fips).to_json, headers: { 'Content-Type' => 'application/json', 'x-domo-embed-token' => @token } ))
      end
    end

    private 
    def get_token
      token = /x-domo-embed-token': '(.*)'/.match(HTTParty.get(TOKEN_URL).body)[1]
    end

    def get_data_from_response response
      response['chart']['datasources'].values[0]['data']['rows']
    end

    # Hsh is in format of metric => [date = x, date+1 = 1, etc]
    # Pivots to be in format of date => fips => {metric1 = a, metric2 = b}
    def county_transpose_response(hash, fips)
      DATE_RANGE.each do |date|
        formatted_date = date.strftime("%Y-%m-%d")
        @data[formatted_date] = {} unless @data.has_key? formatted_date
        @data[formatted_date][fips] = {} 
         
        hash.each do |metric, response_hash|
          metric_val = response_hash.find {|arr| arr[0] == formatted_date} || [0,0]
          @data[formatted_date][fips][metric] = metric_val[1]
        end
        hash
      end
    end
  end
end
