require 'bundler/setup'
require 'optparse'
require 'csv'
require 'json'
require 'pry'
require 'active_support/core_ext/hash/deep_merge.rb'

require_relative('../lib/constants.rb')

Options = Struct.new(
  :file,
  :type
)

class Converter

  attr_reader :file, :type

  def initialize(options)
    @file = options.file
    @type = options.type
  end

  def convert
    case type
    when 'county'
      convert_county_csv
    when 'state'
      convert_state_csv
    end
  end

  def convert_state_csv
    json_data = Hash.new { |hash, key| hash[key] = {} }

    csv_data = CSV.parse(File.read(file), headers: true)

    csv_data.each do |row|
      json_data[row['Date']]['total_tests'] = row['State - Total Tests']
      json_data[row['Date']]['positive_tests'] = row['State - Positive Tests']
      json_data[row['Date']]['negative_tests'] = row['State - Negative Tests']
      json_data[row['Date']]['individuals_tested'] = row['State - Individuals Tested']
      json_data[row['Date']]['individuals_positive'] = row['State - Individuals Positive']
      json_data[row['Date']]['individuals_negative'] = row['State - Individuals Negative']
      json_data[row['Date']]['deaths'] = row['State - Deaths']
    end

    file_date = file.match(/.*state-totals-(.*)\.csv/)[1]  

    File.open("data/state-totals-#{file_date}.json","w") do |f|
      f.write(json_data.to_json)
    end

  end

  # Target format is date -> county -> metrics key -> value
  def convert_county_csv
    json_data = Hash.new { |hash, key| hash[key] = {} }
    county_metrics = ['total_tests', 'positive_tests', 'negative_tests', 'individuals_tested', 'individuals_positive', 'individuals_negative']

    csv_data = CSV.parse(File.read(file), headers: true)

    csv_data.each do |row|
      CovidIowa::Constants::FIPS_CODES.each do |code, data|
        json_data[row['date']][code] = {}

        json_data[row['date']][code]['total_tests'] = row["#{code} - #{data[:name]} - total_tests"]
        json_data[row['date']][code]['positive_tests'] = row["#{code} - #{data[:name]} - positive_tests"]
        json_data[row['date']][code]['negative_tests'] = row["#{code} - #{data[:name]} - negative_tests"]
        json_data[row['date']][code]['individuals_tested'] = row["#{code} - #{data[:name]} - individuals_tested"]
        json_data[row['date']][code]['individuals_positive'] = row["#{code} - #{data[:name]} - individuals_positive"]
        json_data[row['date']][code]['individuals_negative'] = row["#{code} - #{data[:name]} - individuals_negative"]
      end
    end

    file_date = file.match(/.*county-totals-(.*)\.csv/)[1]  

    File.open("data/county-totals-#{file_date}.json","w") do |f|
      f.write(json_data.to_json)
    end
  end

end


if $0 == __FILE__
  # Dir["./data/county-totals-*.csv"].each do |file_string|
  #   options = Options.new
  #   options.type = 'county'
  #   options.file = File.expand_path(file_string)

  #   engine = Converter.new(options)
  #   engine.convert
  # end

  Dir["./data/state-totals-*.csv"].each do |file_string|
    options = Options.new
    options.type = 'state'
    options.file = File.expand_path(file_string)

    engine = Converter.new(options)
    engine.convert
  end
end
