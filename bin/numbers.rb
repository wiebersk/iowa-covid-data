require 'bundler/setup'
require 'optparse'
require 'csv'
require 'pry'
require 'json'
require 'active_support/core_ext/hash/deep_merge.rb'

require_relative('../lib/tests_extractor.rb')
require_relative('../lib/individuals_extractor.rb')
require_relative('../lib/deaths_extractor.rb')
require_relative('../lib/constants.rb')

Options = Struct.new(
  :level
)


class NumbersOptionsParser
  def self.parse(argv)
    args = Options.new

    OptionParser.new do |opts|
      opts.banner = "Usage: #{__FILE__} [options]\n\n"

      opts.on('-l', '--level LEVEL', String, 'State or county data') do |value|
        args.level = value
      end

      opts.on('-h', '--help', 'Print help message') do
        $stdout.puts opts
        exit
      end
    end.parse!(argv)

    args
  end
end

class Numbers

  attr_reader :level

  STATE_HEADERS = ['Date', 'State - Total Tests', 'State - Positive Tests', 'State - Negative Tests', 'State - Individuals Tested', 'State - Individuals Positive', 'State - Individuals Negative', 'State - Deaths']
  START_DATE = '2020-03-11'

  def initialize(options)
    @level = options.level
  end

  def get_numbers
    case level
    when 'state' 
      get_state_numbers
    when 'county'
      get_county_numbers
    end
  end

  private
  
  def get_state_numbers
    tests = CovidIowa::TestsExtractor.new()
    individuals = CovidIowa::IndividualsExtractor.new()
    deaths = CovidIowa::DeathsExtractor.new()

    tests.extract()
    individuals.extract()
    deaths.extract()

    json_data = Hash.new { |hash, key| hash[key] = {} }

    CSV.open("data/daily/state-totals-#{Date.today.strftime("%Y-%m-%d")}.csv", "w+") do |csv|
      csv << STATE_HEADERS
    
      CovidIowa::Constants::DATE_RANGE.each do |day|
        row_data = []
        formatted_date = day.strftime("%Y-%m-%d")

        puts formatted_date
        row_data << formatted_date
        val = tests.data['iowa']['total_tests'].find {|arr| arr[0] == formatted_date} || [0,0]
        json_data[formatted_date]['total_tests'] = val[1]
        row_data << val[1]
        val = tests.data['iowa']['positive_tests'].find {|arr| arr[0] == formatted_date} || [0,0]
        json_data[formatted_date]['positive_tests'] = val[1]
        row_data << val[1]
        val = tests.data['iowa']['negative_tests'].find {|arr| arr[0] == formatted_date} || [0,0]
        json_data[formatted_date]['negative_tests'] = val[1]
        row_data << val[1]

        val = individuals.data['iowa']['individuals_tested'].find {|arr| arr[0] == formatted_date} || [0,0]
        json_data[formatted_date]['individuals_tested'] = val[1]
        row_data << val[1]
        val = individuals.data['iowa']['individuals_positive'].find {|arr| arr[0] == formatted_date} || [0,0]
        json_data[formatted_date]['individuals_positive'] = val[1]
        row_data << val[1]
        val = individuals.data['iowa']['individuals_negative'].find {|arr| arr[0] == formatted_date} || [0,0]
        json_data[formatted_date]['individuals_negative'] = val[1]
        row_data << val[1]

        # Data is date, rolling average, daily deaths
        val = deaths.data['iowa']['deaths'].find {|arr| arr[0] == formatted_date} || [0,0,0]
        json_data[formatted_date]['deaths'] = val[2]
        row_data << val[2]

        csv << row_data
      end
    end

    File.open("data/daily/state-totals-#{Date.today.strftime("%Y-%m-%d")}.json","w") do |f|
      f.write(json_data.to_json)
    end

  end

  def get_county_numbers
    tests = CovidIowa::TestsExtractor.new()
    individuals = CovidIowa::IndividualsExtractor.new()

    headers = ['date']
    county_metrics = ['total_tests', 'positive_tests', 'negative_tests', 'individuals_tested', 'individuals_positive', 'individuals_negative']

    CovidIowa::Constants::FIPS_CODES.each do |code, data|
      puts code
      county_metrics.each do |metric|
        headers << "#{code} - #{data[:name]} - #{metric}"
      end
      fips_tests = tests.extract(code)
      fips_individuals = individuals.extract(code)
    end

    json_data = tests.data.deep_merge(individuals.data)

    File.open("data/daily/county-totals-#{Date.today.strftime("%Y-%m-%d")}.json","w") do |f|
      f.write(json_data.to_json)
    end

    json_data = nil

    CSV.open("data/daily/county-totals-#{Date.today.strftime("%Y-%m-%d")}.csv", "w+") do |csv|
      csv << headers

      CovidIowa::Constants::DATE_RANGE.each do |day|
        formatted_date = day.strftime('%Y-%m-%d')
        row_arr = [formatted_date]

        CovidIowa::Constants::FIPS_CODES.each do |code, _|
          row_arr << tests.data[formatted_date][code]['total_tests']
          row_arr << tests.data[formatted_date][code]['positive_tests']
          row_arr << tests.data[formatted_date][code]['negative_tests']

          row_arr << individuals.data[formatted_date][code]['individuals_tested']
          row_arr << individuals.data[formatted_date][code]['individuals_positive']
          row_arr << individuals.data[formatted_date][code]['individuals_negative']
        end

        csv << row_arr
      end
    end
    

  end
end

if $0 == __FILE__
  options = NumbersOptionsParser.parse(ARGV)

  engine = Numbers.new(options)
  engine.get_numbers
end
